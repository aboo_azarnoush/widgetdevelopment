CREATE TABLE [dbo].[EntityType]
(
[id] [int] NOT NULL,
[name] [nvarchar] (50) COLLATE Latin1_General_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[EntityType] ADD CONSTRAINT [PK__EntityTy__3213E83FB17BF455] PRIMARY KEY CLUSTERED  ([id]) ON [PRIMARY]
GO
